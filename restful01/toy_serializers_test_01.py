from datetime import datetime
from django.utils import timezone
from django.utils.six import BytesIO
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from toys.models import Toy
from toys.serializers import ToySerializer

toy_release_date = timezone.make_aware(datetime.now(), timezone=timezone.get_current_timezone)
toy1 = Toy(name ='Snoppy talking action figure', description='Snoopy speaks five languages', release_date=toy_release_date, toy_category='Action figures', was_included_in_home=False)
toy1.save()

print(toy1.pk)
print(toy1.name)
print(toy1.release_date)
print(toy1.created)

serializer_for_toy1 = ToySerializer(toy1)
print(serializer_for_toy1.data)